import React from "react";
import { Col, Container, Row } from "react-bootstrap";

function ReactContainer() {
  return (
    <Container fluid="md">
      <Container fluid="lg" className="text-secondary">
        <Row className="justify-content-md-center">
          <Col> 1 Col</Col>
          <Col> 2 Col</Col>
          <Col> 3 Col</Col>
        </Row>
        {/* <Row>
        <Col> 1 Col</Col>
        <Col xs={5}> 2 Col</Col>
        <Col> 3 Col</Col>
      </Row> */}
      </Container>

      <Container fluid="xl">
        <Row className="justify-content-md-center">
          <Col> 1 Col</Col>
          <Col> 2 Col</Col>
          <Col> 3 Col</Col>
        </Row>
        {/* <Row>
        <Col> 1 Col</Col>
        <Col xs={5}> 2 Col</Col>
        <Col> 3 Col</Col>
      </Row> */}
      </Container>
    </Container>
  );
}

export default ReactContainer;
