import React from "react";
import { Col, Container, Jumbotron, Row } from "react-bootstrap";
import CheckO2 from "./CheckO2";
import "./LandingPage.css";

function LandingPage() {
  return (
    <Container fluid="xs" className="p-3 border border-light customHeight">
      <Jumbotron className="text-center">
        <Row>
          <Col></Col>
          <Col>
            <CheckO2 />
          </Col>
        </Row>
        {/* <Row className="text-center">
          <Col xs={6} className="text-right">
            <p> Hello</p>
          </Col>
          <Col></Col>
        </Row>
        <Row className="text-center">
          <Col></Col>
          <Col xs={7} className="text-left">
            <p>World</p>
          </Col>
        </Row> */}
      </Jumbotron>
    </Container>
  );
}

export default LandingPage;
