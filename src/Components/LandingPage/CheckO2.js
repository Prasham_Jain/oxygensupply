import React, { useState } from "react";
import { Card, Container, Row, Col } from "react-bootstrap";

import ReactCardFlip from "react-card-flip";

const fontSize = {
  fontSize: "8vw !important",
};
const lineHeight = {
  lineHeight: "0.8",
};

const cardStyle = {
  minHeight: "15rem",
  pading: "10%",
  border: "none",
  backgroundColor: "inherit",
  userSelect: "none",
};

function CheckO2() {
  const [flipped, setFlipped] = useState(false);

  return (
    //use bg="" instead of className="bg"
    <ReactCardFlip isFlipped={flipped} flipDirection="vertical">
      <Card
        className="frontCard"
        style={cardStyle}
        onClick={() => {
          setFlipped(!flipped);
        }}
      >
        <Card.Body>
          <Row>
            <Col
              className="display-4 pl-lg-5 text-muted text-left"
              style={lineHeight}
            >
              Check
            </Col>
          </Row>
          <Row>
            <Col
              className="display-2 pl-lg-5 text-muted text-left"
              style={lineHeight}
            >
              Oxygen...
            </Col>
          </Row>
        </Card.Body>
      </Card>

      <Card
        className="backCard"
        style={cardStyle}
        onClick={() => {
          setFlipped(!flipped);
        }}
      >
        <Card.Body>
          <Row>
            <Col
              className="display-4 pl-lg-5 text-muted text-left"
              style={lineHeight}
            >
              Get Vaccinated
            </Col>
          </Row>
          <Row>
            <Col
              className="display-2 pl-lg-5 text-muted text-left"
              style={lineHeight}
            >
              Break The Chain...
            </Col>
          </Row>
        </Card.Body>
      </Card>
    </ReactCardFlip>
  );
}

export default CheckO2;
